#!/usr/bin/env sh
env
MODULE_VERSION=$(python -c 'import demomodule;print(demomodule.__version__)')
if [ "${CI_BUILD_TAG}" != "${MODULE_VERSION}" ] ; then
    echo "${CI_BUILD_TAG} does not match ${MODULE_VERSION}"
    exit 1
else
    echo "Publishing version ${MODULE_VERSION}"
fi
pip install twine
python setup.py sdist bdist_wheel
twine upload --repository-url https://${NEXUS_HOST}/repository/pypi-internal/ -u ${NEXUS_USR} -p ${NEXUS_PSW} dist/*
