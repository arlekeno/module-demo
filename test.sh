#!/usr/bin/env sh
env
if [ "${NEXUS_HOST}" != "" ]; then
echo "configuring pip.conf"
mkdir -p $HOME/.config/pip
cat > $HOME/.config/pip/pip.conf <<EOF
[global]
index = https://${NEXUS_USR}:${NEXUS_PSW}@${NEXUS_HOST}/repository/pypi-all/pypi
index-url = https://${NEXUS_USR}:${NEXUS_PSW}@${NEXUS_HOST}/repository/pypi-all/simple
EOF
fi
cat $HOME/.config/pip/pip.conf
pip install pytest
pytest
