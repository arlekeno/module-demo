#!/usr/bin/env python

"""
distutils/setuptools install script.
"""
import os
import re

from setuptools import setup, find_packages


ROOT = os.path.dirname(__file__)
VERSION_RE = re.compile(r'''__version__ = ['"]([0-9.]+)['"]''')


requires = [
    'boto3'
]


def get_version():
    init = open(os.path.join(ROOT, 'demomodule', '__init__.py')).read()
    return VERSION_RE.search(init).group(1)


setup(
    name='demomodule',
    version=get_version(),
    description='Example of building and publishing a python module',
    long_description=open('README.rst').read(),
    author='andrew.stephenson@gmail.com',
    url='https://dontbother.com',
    scripts=[],
    packages=find_packages(exclude=['tests*']),
    package_data={
    },
    include_package_data=True,
    install_requires=requires,
    license="Apache License 2.0",
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)